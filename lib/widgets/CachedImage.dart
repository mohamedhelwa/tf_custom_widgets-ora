import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:tf_custom_widgets/res.dart';
import 'package:tf_custom_widgets/utils/WidgetUtils.dart';

class CachedImage extends StatelessWidget {
  final String url;
  final BoxFit? fit;
  final double? height, width;
  final BorderRadius? borderRadius;
  final ColorFilter? colorFilter;
  final Alignment? alignment;
  final Widget? child;
  final Widget? placeHolder;
  final Color? borderColor;
  final Color? bgColor;
  final BoxShape? boxShape;
  final bool haveRadius;
  final bool? clearMemoryCache;
  final String? hash;

  const CachedImage({
    Key? key,
    required this.url,
    this.fit,
    this.width,
    this.height,
    this.placeHolder,
    this.borderRadius,
    this.colorFilter,
    this.alignment,
    this.child,
    this.boxShape,
    this.borderColor,
    this.bgColor,
    this.haveRadius = true,
    this.clearMemoryCache = true,
    this.hash,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (clearMemoryCache != null) {
      if (clearMemoryCache!) {
        // _checkMemory();
      }
    }

    return url.startsWith("http")
        ? CachedNetworkImage(
            imageUrl: "$url",
            width: width,
            height: height,
            imageBuilder: (context, imageProvider) => Container(
              width: width,
              height: height,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: imageProvider,
                    fit: fit ?? BoxFit.fill,
                    colorFilter: colorFilter),
                borderRadius: haveRadius
                    ? borderRadius ?? BorderRadius.circular(0)
                    : null,
                shape: boxShape ?? BoxShape.rectangle,
                border: Border.all(
                    color: borderColor ?? Colors.transparent, width: 1),
              ),
              alignment: alignment ?? Alignment.center,
              child: child,
            ),
            placeholder: (context, url) => Container(
              width: width,
              height: height,
              alignment: Alignment.center,
              child: haveRadius
                  ? ClipRRect(
                      clipBehavior: Clip.hardEdge,
                      borderRadius: borderRadius ?? BorderRadius.circular(0),
                      // child: BlurHash(
                      //   hash: hash ?? "UcHn]FRjD%of?wWBIUj[Xnj[V@s:xvRkjFs:",
                      //   imageFit: fit ?? BoxFit.fill,
                      // ),
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey.shade300,
                        highlightColor: Colors.grey.shade100,
                        child: Container(
                          width: width,
                          height: height,
                          color: Colors.white,
                        ),
                      ),
                    )
                  : Container(
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: borderColor ?? Colors.transparent, width: 1),
                        shape: boxShape ?? BoxShape.rectangle,
                        color:
                            bgColor ?? WidgetUtils.primaryColor.withOpacity(.5),
                      ),
                      // child: BlurHash(
                      //   hash: hash ?? "UcHn]FRjD%of?wWBIUj[Xnj[V@s:xvRkjFs:",
                      //   imageFit: fit ?? BoxFit.fill,
                      // ),
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey.shade300,
                        highlightColor: Colors.grey.shade100,
                        child: Container(
                          width: width,
                          height: height,
                          color: Colors.white,
                        ),
                      ),
                    ),
            ),
            errorWidget: (context, url, error) {
              return Container(
                width: width,
                height: height,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: bgColor ?? WidgetUtils.primaryColor.withOpacity(.5),
                  borderRadius: haveRadius
                      ? borderRadius ?? BorderRadius.circular(0)
                      : null,
                  border: Border.all(
                      color: borderColor ?? Colors.transparent, width: 1),
                  shape: boxShape ?? BoxShape.rectangle,
                ),
                child: Stack(
                  children: [
                    placeHolder ?? child ?? Container(),
                    placeHolder ?? Center(child: Image.asset(Res.placeholder)),
                  ],
                ),
              );
            },
          )
        : ClipRRect(
            borderRadius: haveRadius
                ? borderRadius ?? BorderRadius.circular(0)
                : BorderRadius.circular(0),
            child: Container(
              width: width,
              height: height,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: bgColor ?? WidgetUtils.primaryColor.withOpacity(.5),
                borderRadius: haveRadius
                    ? borderRadius ?? BorderRadius.circular(0)
                    : null,
                border: Border.all(
                    color: borderColor ?? Colors.transparent, width: 1),
                shape: boxShape ?? BoxShape.rectangle,
                image: DecorationImage(
                  image: MemoryImage(
                    Base64Decoder().convert(url.split(',').last),
                  ),
                  fit: fit ?? BoxFit.fill,
                  colorFilter: colorFilter,
                ),
              ),
              child: child,
            ),
          );
  }

  void _checkMemory() {
    ImageCache imageCache = PaintingBinding.instance.imageCache;
    if (imageCache.currentSizeBytes >= 55 << 20) {
      print('##### Clearing image cache #####');
      print("Cache size: ${imageCache.currentSizeBytes}");
      imageCache.clear();
      imageCache.clearLiveImages();
      print("Current Cache Size: ${imageCache.currentSizeBytes}");
    }
  }
}
